function renderProduct(productArr) {
  var contentHTML = "";
  for (var i = 0; i < productArr.length; i++) {
    var product = productArr[i];
    var contentDiv = `
            <div class="mx-3">                
                <div class="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    <a href="#">
                        <img class="p-8 rounded-t-lg" src="${product.img}" alt="product image" />
                    </a>
                    <div class="px-5 pb-5">
                        <a href="#">
                            <h5 class="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">${product.name}</h5>
                        </a>
                        <div class="flex items-center justify-between">
                            <span class="text-3xl font-bold text-gray-900 dark:text-white">${product.price}</span>
                            <a href="#" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" onclick = "addCart(${product.id})">Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        `;
    contentHTML += contentDiv;
  }
  document.getElementById("product").innerHTML = contentHTML;
}

function renderCart(cartItems) {
  var contentHTML = "";
  var totalPrice = 0; // Tổng tiền

  for (var i = 0; i < cartItems.length; i++) {
    var cartItem = cartItems[i];
    var contentCart = `
      <div class="bg-white rounded-lg p-4 mb-4" id="cartItem-${cartItem.id}">
        <div class="flex items-center">
          <img src="${cartItem.img}" alt="Product Image" class="w-20 h-20 mr-4 rounded-lg">
          <div>
            <p class="text-xl font-semibold">Tên SP: ${cartItem.name}</p>
            <p class="text-gray-600">Giá: ${cartItem.price}</p>
            <div class="flex items-center mt-2">
              <button class="text-gray-500 hover:text-gray-700 mr-2" onclick="decreaseCartItem(${cartItem.id})">
                <svg class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4"></path>
                </svg>
              </button>
              <p class="text-gray-600">Số lượng: ${cartItem.quality}</p>
              <button class="text-gray-500 hover:text-gray-700 ml-2" onclick="increaseCartItem(${cartItem.id})">
                <svg class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path>
                </svg>
              </button>
              <button class="text-red-500 hover:text-red-700 ml-4" onclick="removeCartItem(${cartItem.id})">
                <svg class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                </svg>      
              </button>
            </div>
          </div>
        </div>
      </div>
    `;
    contentHTML += contentCart;
    // Cộng dồn giá tiền
    totalPrice += cartItem.price * cartItem.quality;
  }

  // Thêm dòng tổng tiền vào HTML
  var totalHTML = `
    <div class="text-right mt-4">
      <p class="text-xl font-semibold">Tổng tiền: ${totalPrice}</p>
    </div>
  `;
  contentHTML += totalHTML;

  document.getElementById("cartRender").innerHTML = contentHTML;
}
