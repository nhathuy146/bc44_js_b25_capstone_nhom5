const BASE_URL = "https://6457c6440c15cb148210c89c.mockapi.io/product";
var cartItems = [];

var dataJson = localStorage.getItem("CARTITEMS_LOCAL");
if (dataJson != null) {
  cartItems = JSON.parse(dataJson);
  renderCart(cartItems);
}
// Lấy dữ liệu và render
axios({
  url: BASE_URL,
  method: "GET",
})
  .then(function (res) {
    console.log(res.data);
    renderProduct(res.data);
  })
  .catch(function (err) {
    console.log(err);
  });

// filter
function filterProduct() {
  var brand = document.getElementById("brand").value;
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then(function (res) {
      var filteredProduct = "";
      var products = res.data;
      // Lọc sản phẩm theo brand
      if (brand === "0") {
        filteredProduct = products;
      } else {
        filteredProduct = products.filter(function (product) {
          return product.type === brand;
        });
        console.log(
          `🚀 ~ filteredProducts ~ filteredProducts:`,
          filteredProduct
        );
      }
      // Render sản phẩm đã lọc
      renderProduct(filteredProduct);
    })
    .catch(function (err) {
      console.log(err);
    });
}

// Modal
var closeModalButton = document.getElementById("closeModalButton");
var cartModal = document.getElementById("cartModal");
var cartButton = document.querySelector(".flex button");

cartButton.addEventListener("click", function () {
  cartModal.classList.toggle("opacity-0");
  cartModal.classList.toggle("pointer-events-none");
});

closeModalButton.addEventListener("click", function () {
  cartModal.classList.toggle("opacity-0");
  cartModal.classList.toggle("pointer-events-none");
});

// Add cart
function addCart(productId) {
  axios({
    url: `${BASE_URL}/${productId}`,
    method: "GET",
  })
    .then(function (res) {
      var apiData = res.data;

      // Lấy thông tin từ API
      var id = apiData.id;
      var name = apiData.name;
      var price = apiData.price;
      var img = apiData.img;
      var quality = 1; // quality mặc định là 1

      // Kiểm tra xem sản phẩm đã tồn tại trong giỏ hàng chưa
      var existingCartItemIndex = -1;
      for (var i = 0; i < cartItems.length; i++) {
        if (cartItems[i].id === id) {
          existingCartItemIndex = i;
          console.log(`🚀 ~ existingCartItemIndex:`, existingCartItemIndex);
          break;
        }
      }

      // Nếu sản phẩm đã tồn tại, tăng số lượng lên 1
      if (existingCartItemIndex !== -1) {
        cartItems[existingCartItemIndex].quality += 1;
        console.log(`🚀 ~ cartItems:`, cartItems);
      } else {
        // Nếu sản phẩm chưa tồn tại, thêm vào giỏ hàng
        // Tạo đối tượng cartItem
        var cartItem = new CartItem(id, name, price, img, quality);
        console.log("CartItem:", cartItem);
        cartItems.push(cartItem);
        saveCartToLocalStorage();
      }

      // Render giỏ hàng
      totalQuality(cartItems);
      renderCart(cartItems);
    })
    .catch(function (err) {
      console.log(err);
    });
}

// Tăng, giảm, xóa giỏ hàng

function increaseCartItem(cartItemId) {
  // Tìm kiếm sản phẩm trong mảng cartItems dựa trên cartItemId
  for (var i = 0; i < cartItems.length; i++) {
    // console.log(`🚀 ~ increaseCartItem ~ cartItemId:`, typeof cartItemId);
    // console.log(`🚀 ~ increaseCartItem ~ cartItems:`, typeof cartItems[i].id);
    var stringCartItemId = cartItemId.toString();
    if (cartItems[i].id === stringCartItemId) {
      // Tăng số lượng sản phẩm lên 1
      cartItems[i].quality += 1;

      // Render lại giỏ hàng
      totalQuality(cartItems);
      renderCart(cartItems);
    }
    console.log(`🚀 ~ increaseCartItem ~ cartItems:`, cartItems);
  }
  saveCartToLocalStorage();
}

function decreaseCartItem(cartItemId) {
  // Tìm kiếm sản phẩm trong mảng cartItems dựa trên cartItemId
  for (var i = 0; i < cartItems.length; i++) {
    var stringCartItemId = cartItemId.toString();
    if (cartItems[i].id === stringCartItemId) {
      // Giảm số lượng sản phẩm đi 1, nhưng không nhỏ hơn 1
      cartItems[i].quality = Math.max(1, cartItems[i].quality - 1);
      // Render lại giỏ hàng
      totalQuality(cartItems);
      renderCart(cartItems);
    }
    console.log(`🚀 ~ decreaseCartItem ~ cartItems:`, cartItems);
  }
  saveCartToLocalStorage();
}

function removeCartItem(cartItemId) {
  // Tìm kiếm sản phẩm trong mảng cartItems dựa trên cartItemId
  for (var i = 0; i < cartItems.length; i++) {
    var stringCartItemId = cartItemId.toString();
    if (cartItems[i].id === stringCartItemId) {
      // Xóa sản phẩm khỏi giỏ hàng
      cartItems.splice(i, 1);
      // Render lại giỏ hàng
      totalQuality(cartItems);
      renderCart(cartItems);
    }
    saveCartToLocalStorage();
  }
}

function clearCart() {
  // Cập nhật giao diện giỏ hàng rỗng
  cartItems = [];
  totalQuality(cartItems);
  renderCart(cartItems);
  saveCartToLocalStorage();
  console.log(`🚀 ~ clearCart ~ cartItems:`, cartItems);
  document.getElementById("cartRender").innerHTML = "Thanh toán thành công";
}
// Set json
function saveCartToLocalStorage() {
  // Chuyển đổi mảng cartItems thành chuỗi JSON
  var dataJson = JSON.stringify(cartItems);
  // Lưu chuỗi JSON vào Local Storage
  localStorage.setItem("CARTITEMS_LOCAL", dataJson);
}

// In số lượng sp ra giao diện
function totalQuality(cartItems) {
  var spTotal = document.getElementById("totalQuality");
  var totalQuality = 0;
  for (var i = 0; i < cartItems.length; i++) {
    totalQuality += cartItems[i].quality;
  }
  // Nếu số lượng =0 thì ẩn dấu tròn đỏ trên icon cart
  if (totalQuality == 0) {
    spTotal.classList.add("hidden");
  } else {
    spTotal.classList.remove("hidden");
  }

  document.getElementById("totalQuality").innerText = totalQuality;
}
